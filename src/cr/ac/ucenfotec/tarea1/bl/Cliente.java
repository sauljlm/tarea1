package cr.ac.ucenfotec.tarea1.bl;

import java.util.Objects;

public class Cliente {
    private String nompre;
    private String apellido1;
    private String apellido2;
    private String direccion;
    private String correo;

    public Cliente(String nompre,String apellido1,String apellido2, String direccion, String correo) {
        setNombre(nompre);
        setApellido1(apellido1);
        setApellido2(apellido2);
        setDireccion(direccion);
        setCorreo(correo);
    }

    public String getNombre() { return nompre; }
    public String getApellido1() {
        return apellido1;
    }
    public String getApellido2() {
        return apellido2;
    }
    public String getDireccion() {
        return direccion;
    }
    public String getCorreo() {
        return correo;
    }
    public void setNombre(String nompre) {
        this.nompre = nompre;
    }
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(getCorreo(), cliente.getCorreo());
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "Correo electronico=" + correo +
                '}';
    }
}
