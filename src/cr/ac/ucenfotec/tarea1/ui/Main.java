package cr.ac.ucenfotec.tarea1.ui;

import cr.ac.ucenfotec.tarea1.bl.Cliente;
import cr.ac.ucenfotec.tarea1.bl.Camiseta;

import java.io.*;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Cliente> clientes = new ArrayList<>();
    static ArrayList<Camiseta> camisetas = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        int opcion = 0;
        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarCliente();
                    break;
                case 2:
                    agregarCamiseta();
                    break;
                case 3:
                    verClientes();
                    break;
                case 4:
                    verCamisetas();
                    break;
            }
        } while (opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("--- MENU ---");
        out.println("1. Agregar Cliente");
        out.println("2. Agregar Camiseta");
        out.println("3. Ver Clientes");
        out.println("3. Ver Camisetas");
        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void agregarCliente() throws IOException {
        out.println("Agregar cliente");
        out.println();
        out.print("Digite el nombre: ");
        String nombre = in.readLine();
        out.print("Digite el primer apellido: ");
        String apellido1 = in.readLine();
        out.print("Digite el segundo apellido: ");
        String apellido2 = in.readLine();
        out.print("Digite la direccion exacta: ");
        String direccion = in.readLine();
        out.print("Digite el correo electronico: ");
        String correo = in.readLine();
        Cliente clienteActual = new Cliente(nombre, apellido1, apellido2, direccion, correo);
        if (!checkClientes(clienteActual)) {
            clientes.add(clienteActual);
        }
    }

    public static void agregarCamiseta() throws IOException {
        out.println("Agregar camiseta");
        out.println();
        out.print("Digite el ID: ");
        String id = in.readLine();
        out.print("Digite el codigo de color: ");
        String color = in.readLine();
        out.print("Digite el tamaño: ");
        String tamanio = in.readLine();
        out.print("Digite la descripcion: ");
        String descripcion = in.readLine();
        out.print("Digite el precio: ");
        double precio = Double.parseDouble(in.readLine());
        Camiseta camisetaActual = new Camiseta(id, color, tamanio, descripcion, precio);
        if (!checkCamisetas(camisetaActual)) {
            camisetas.add(camisetaActual);
        }
    }

    public static void verClientes() {
        for (int i = 0; i < clientes.size(); i++) {
            Cliente cliente = clientes.get(i);
            out.println(i + ") " + cliente.toString());
            out.println("Nombre:" + cliente.getNombre());
            out.println("Primer apellido:" + cliente.getApellido1());
            out.println("Segundo apellido:" + cliente.getApellido2());
            out.println("Direccion exacta:" + cliente.getDireccion());
            out.println("Correo electronico:" + cliente.getCorreo());
            out.println("");
        }
    }

    public static void verCamisetas() {
        for (int i = 0; i < camisetas.size(); i++) {
            Camiseta camiseta = camisetas.get(i);
            out.println(i + ") " + camiseta.toString());
            out.println("ID:" + camiseta.getID());
            out.println("Color:" + camiseta.getColor());
            out.println("Tamaño:" + camiseta.getTamanio());
            out.println("Descripcion:" + camiseta.getDescripcion());
            out.println("Precio:" + camiseta.getPrecio());
            out.println("");
        }
    }

    public static boolean checkClientes(Cliente clienteActual) {
        boolean existe = false;
        for (int i = 0; i < clientes.size(); i++) {
            if (clientes.get(i).equals(clienteActual)) {
                existe = true;
            }
        }
        return  existe;
    }

    public static boolean checkCamisetas(Camiseta camisetaActual) {
        boolean existe = false;
        for (int i = 0; i < camisetas.size(); i++) {
            if (camisetas.get(i).equals(camisetaActual)) {
                existe = true;
            }
        }
        return  existe;
    }
}
