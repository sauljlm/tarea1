package cr.ac.ucenfotec.tarea1.bl;

import java.util.Objects;

public class Camiseta {
    private String id;
    private String color;
    private String tamanio;
    private String descripcion;
    private double precio;

    public Camiseta(String id,String color,String tamanio, String descripcion, double precio) {
        setID(id);
        setColor(color);
        setTamanio(tamanio);
        setDescripcion(descripcion);
        setPrecio(precio);
    }

    public String getID() { return id; }
    public String getColor() {
        return color;
    }
    public String getTamanio() {
        return tamanio;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public double getPrecio() {
        return precio;
    }
    public void setID(String id) {
        this.id = id;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Camiseta camiseta = (Camiseta) o;
        return Objects.equals(id, camiseta.id);
    }

    @Override
    public String toString() {
        return "Camiseta{" +
                "ID= " + id +
                '}';
    }
}
